# Board for Design Team


The main purpose of this repository is to issue tickets on it's respective boards for Todo tasks and Done tasks


We can throw all figma changes to here. This can serve as logging also. So PMs can check what are new changes in the Designs. <br>
Open are tasks to do. <br>
Doing are tasks currently in progress. <br>
New Changes are tasks reflected in Figma  <br>
Closed are tasks reflected in the codes


Approver:
https://gitlab.com/dctx/rapidpass/rapidpass-design/-/boards/1631640

Checkpoint:
https://gitlab.com/dctx/rapidpass/rapidpass-design/-/boards/1631641

Registration:
https://gitlab.com/dctx/rapidpass/rapidpass-design/-/boards/1631638


Note: you can change board  at the top of gitlab, below the breadcrumbs, beside 'Search or filter results...'